#!/bin/sh

choices="襤 Poweroff| Suspend| Reboot"

choice=$(echo "$choices" | rofi -sep '|' -dmenu -dpi 200 | tr '\n' ' ')


case $choice in
    "襤 Poweroff ")
        echo "Initiated poweroff"
        systemctl poweroff;;
    " Suspend ")
        echo "Initiated suspend"
        systemctl suspend;;
    " Reboot ")
        echo "Initiated reboot"
        systemctl reboot;;
    *)
        echo "Option not recognized";;
esac


