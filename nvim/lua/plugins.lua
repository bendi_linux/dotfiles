return require('packer').startup(function(use)
    
    use 'wbthomason/packer.nvim' --Base Plugin manager

    use 'morhetz/gruvbox' -- Gruvbox theme

    use 'itchyny/lightline.vim' --Minimalist status line

    use 'neovim/nvim-lspconfig' --LSP plugin

    use 'preservim/nerdtree' --File tree

    use 'ap/vim-css-color' --css color preview

    use {'ms-jpq/coq_nvim', run = "python3 -m coq deps"} --autocomplete
    use 'ms-jpq/coq.artifacts'
    use 'ms-jpq/coq.thirdparty'

    use { "williamboman/mason.nvim" } --lsp installer 

    use 'ryanoasis/vim-devicons'

end)
