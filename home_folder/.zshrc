autoload -U colors && colors

PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$fg[cyan]%}%B%(!/./❯)%b "


# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' format '%d'
zstyle ':completion:*' group-name ''
zstyle :compinstall filename '/home/david/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

export EDITOR="nvim"


# End of lines configured by zsh-newuser-install
alias ls="ls -a --color=auto"
alias cp="cp -v"
alias la="exa --classify --all --group-directories-first --color=auto"
alias exa="exa --all --classify --color=auto --long"
